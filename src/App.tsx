import "./App.css";
import Heading from "./components/Heading";
import PerentHeading from "./components/PerentHeading";
import Person from "./components/Person";
import PersonList from "./components/PersonList";
import Status from "./components/Status";
import WelcomeUser from "./components/WelcomeUser";
import Button from "./components/Button";
import Input from "./components/Input";
import Container from "./components/Container";
import UseStateType from "./components/UseStateType";
import AdvancedUseState from "./components/AdvancedUseState";
import UseReducer from "./components/UseReducer";
import { ThemeContextProvider } from "./components/context/ThemeContext";
import { Box } from "./components/context/Box";
import { UserContextProvider } from "./components/context/UserContext";
import User from "./components/context/User";
import DomRef from "./components/ref/DomRef";
import MutableRef from "./components/ref/MutableRef";
import Counter from "./components/class/Counter";
import Private from "./components/class/Private";
import Profile from "./components/class/Profile";
import List from "./components/generic/List";
import RandomNumber from "./components/restriction/RandomNumber";
import Toast from "./components/template literals/Toast";
import HTMLButton from "./components/html/HTMLButton";
import HTMLInput from "./components/html/HTMLInput";
import CustomCompo from "./components/html/CustomCompo";
import Text from "./components/polymorphic/Text";

//typescript is a compiler to check the type of the react code.
function App() {
  const personInfo = {
    firstname: "zenith",
    lastname: "poriya",
  };
  // const listArray = ["zenith","ashokbhai","poriya"]
  const listArray = [
    {
      firstname: "name1",
      lastname: "zenith",
    },
    {
      firstname: "name2",
      lastname: "ashokbhai",
    },
    {
      firstname: "name3",
      lastname: "poriya",
    },
  ];

  return (
    <div className="App">
      {/* <WelcomeUser name="Zenith" isLoggedIn={true} /> */}
      {/* pass object as a props */}
      {/* <Person name={personInfo} /> */}
      {/* pass array as a props of component */}
      {/* <PersonList list={listArray} /> */}
      {/* pass enum props */}
      {/* <Status status="success" /> */}
      {/* pass props as a children of the child component */}
      {/* <Heading>hello, this is zenith.</Heading> */}
      {/* pass the whole component as a childern of component */}
      {/* <PerentHeading>
        <Heading>hello, i'm zenith</Heading>
      </PerentHeading> */}
      {/* Event Props */}
      {/* HandleClick Props */}
      {/* <Button
        handleClick={(event, id) => {
          console.log("button clicked", event, id);
        }}
      ></Button> */}
      {/* HandleChange Props */}
      {/* <Input value="" handleChange={(event) => console.log(event)}></Input> */}
      {/* Style as a props */}
      {/* <Container
        styles={{
          border: "1px solid red",
          padding: "10px",
          display: "inline-block",
        }}
      ></Container> */}
      {/* @@@@@Hooks@@@@ */}
      {/* useState with typescript */}
      {/* <UseStateType /> */}
      {/* <AdvancedUseState /> */}
      {/* useReducer with typescript */}
      {/* <UseReducer /> */}
      {/* type in the useContext API */}
      {/* <ThemeContextProvider>
        <Box />
      </ThemeContextProvider> */}
      {/* type in context but in object way */}
      {/* <UserContextProvider>
        <User />
      </UserContextProvider> */}
      {/* type of ref in component */}
      {/* <DomRef /> */}
      {/* <MutableRef /> */}
      {/* typescript in class component */}
      {/* <Counter message="typescirpt in class component" /> */}
      {/* props type in class component */}
      {/* <Private isLoggedIn={true} Component={Profile} /> */}
      {/* types of generic in class components */}
      {/* <List
        items={[1, 2, 3, 4, 5]}
        onClick={(value) => {
          console.log(value);
        }}
      />
      <List
        items={["zenith", "poriya"]}
        onClick={(value) => {
          console.log(value);
        }}
      /> */}
      {/* restriction in props */}
      {/* without restriction we can add multiple props - so we have to restric the props to write*/}
      {/* <RandomNumber value={10} isPositive={true} /> */}
      {/* Template literals types */}
      {/* <Toast position={"left-bottom"} /> */}
      {/* HTML Wrapping element types  */} {/*remaining - @@*/}
      {/* <HTMLButton
        classname={"primary"}
        onClick={() => console.log("Button clicked")}
      >
        Primary button
      </HTMLButton> */}
      {/* <HTMLInput type="text" className="input-class" /> */}
      {/* extracting the type of another component */}
      {/* <CustomCompo name={"zenith"} isLoggedIn={true} /> */}
      {/* Polymorphic component */}
      {/* <Text as={"h3"} size={"md"} color={"primary"}>
        Welcome, user 1
      </Text>
      <Text as={"h6"} size={"sm"}>
        Welcome, user 2
      </Text>
      <Text as={"h1"} size={"lg"} color={"secondary"}>
        Welcome, user 3
      </Text> */}
    </div>
  );
}

export default App;
