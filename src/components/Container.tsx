type ContainerType = {
  styles: React.CSSProperties;
};
//like this we can added the css properties as a props

const Container = (props: ContainerType) => {
  return (
    <>
      <h4>Pass the stype as a props s</h4>
      <div style={props.styles}>Hello</div>
    </>
  );
};

export default Container;
