import { useState } from "react";

type AuthType = {
  name: string;
};

//like this we can give the type of the state of the function
function AdvancedUseState() {
  //like this we can pass the type of the state
  const [user, setUser] = useState<AuthType | null>(null);

  return (
    <>
      <h4>More then two types in useState</h4>
      <button onClick={() => setUser({ name: "Zenith" })}>Log in</button>
      <button onClick={() => setUser(null)}>Log out</button>
      {user?.name && <p>Welcome {user.name}</p>}
    </>
  );
}

export default AdvancedUseState;
