type PerentHeadingTypes = {
  children: React.ReactNode;
};

const PerentHeading = (props: PerentHeadingTypes) => {
  return (
    <>
      <h4>component as a children pass</h4>
      <div>{props.children}</div>
    </>
  );
};

export default PerentHeading;
