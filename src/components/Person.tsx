import { personType } from "./PersonType";

//return object
//we can destructure the props also
const Person = ({ name }: personType) => {
  return (
    <>
      <h4>Object as a props and it's type</h4>
      <h2>
        {name.firstname} {name.lastname}
      </h2>
    </>
  );
};

export default Person;
