import { Component } from "react";

type CounterState = {
  count: number;
};
type CounterProps = {
  message: string;
};

//if we don't need props then we can used the {empty_object}
//and if you don't need the state then you simply remove it

export default class Counter extends Component<CounterProps, CounterState> {
  //define the state for the counter
  state = {
    count: 0,
  };

  //increment the counter value
  handleIncrement = () => {
    this.setState((prestate) => ({ count: prestate.count + 1 }));
  };

  render() {
    return (
      <>
        <h4>{this.props.message}</h4>
        <p>Count : {this.state.count}</p>
        <button onClick={this.handleIncrement}>Increment</button>
      </>
    );
  }
}
