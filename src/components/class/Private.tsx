import React from "react";
import Login from "./Login";
import { ProfileType } from "./Profile";

//define the type of the props comes with the component
type PrivateProps = {
  isLoggedIn: boolean;

  //if the props is compoenent then we can write like this we have to define the type which component contain
  Component: React.ComponentType<ProfileType>;
};

function Private({ isLoggedIn, Component }: PrivateProps) {
  if (isLoggedIn) {
    return <Component name="zenith" />;
  } else {
    return <Login />;
  }
}

export default Private;
