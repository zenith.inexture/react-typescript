export type ProfileType = {
  name: string;
};

function Profile({ name }: ProfileType) {
  return <div>Name is {name}</div>;
}

export default Profile;
