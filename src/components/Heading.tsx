//like this we can define the type of children of the component
type HeadingProps = {
  children: string;
};

const Heading = (props: HeadingProps) => {
  return (
    <>
      <h4>type of children in component</h4>
      <h2>{props.children}</h2>
    </>
  );
};

export default Heading;
