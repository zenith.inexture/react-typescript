import React from "react";

//if we pass the html tag that are we used to render it's know as polymorphic type
//the type of those props is React.ElementType
type TextProps = {
  size: "sm" | "md" | "lg";
  color?: "primary" | "secondary";
  children: React.ReactNode;
  as?: React.ElementType;
};

function Text({ size, color, children, as }: TextProps) {
  color = color || "primary";
  const Component = as || "div";
  //define the component props or a default component
  return (
    <>
      <h4>Polymorphic component props</h4>
      <Component className={color}>
        Size is : {size}
        <br></br>
        Text is : {children}
      </Component>
    </>
  );
}

export default Text;
