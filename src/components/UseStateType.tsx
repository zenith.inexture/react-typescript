import React, { useState } from "react";

function UseStateType() {
  //typescript is capable enough to refer the type of the state that are created
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  //if we try to pass something else in the state is gives us the error
  //but this is easy to use in only small state and single value in state
  //in multiple value state it's difficult to manage those props type

  return (
    <>
      <h4>useState hook with types</h4>
      <button onClick={() => setIsLoggedIn(true)}>Log in</button>
      <button onClick={() => setIsLoggedIn(false)}>Log out</button>
      <p>User is {isLoggedIn ? "Logged in" : "Logged out"}</p>
    </>
  );
}

export default UseStateType;
