type StatsProps = {
  status: "loading" | "success" | "error";
};
//this is one type of enum type in this
//we can use only this three string to access the props in child component
const Status = (props: StatsProps) => {
  let message;
  if (props.status === "loading") {
    message = "loading....";
  } else if (props.status === "success") {
    message = "data fetched successfully";
  } else if (props.status === "error") {
    message = "Error occoured while fetching the data";
  }
  return (
    <>
      <h4>enum types in props</h4>
      <h2>status : {message}</h2>
    </>
  );
};

export default Status;
