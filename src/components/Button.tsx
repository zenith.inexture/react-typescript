import React from "react";

//if we want to specify the event in onClick function then we have to use the React.MouseEvent<HTMLButtonElement>
type ButtonType = {
  handleClick: (event: React.MouseEvent<HTMLButtonElement>, id: number) => void;
};
//like this we can pass the type of button

const Button = (props: ButtonType) => {
  return (
    <>
      <h4>handleClick function as a props of the children</h4>
      <button onClick={(event) => props.handleClick(event, 21)}>
        Button Click
      </button>
    </>
  );
};

export default Button;
