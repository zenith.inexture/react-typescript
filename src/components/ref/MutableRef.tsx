import { useEffect, useRef, useState } from "react";

function MutableRef() {
  const [Timer, setTimer] = useState(0);

  //define the type of ref is number or null - bcz of the setInterval return the interval id in numbar so
  //we can write the number or undefined also bcz the clearInterval return the undefined value
  const timerRef = useRef<number | null>(null);

  const stopTimer = () => {
    if (timerRef.current) window.clearInterval(timerRef.current);
    //or you can pass the type of the useRef is number | undefined
  };

  //start and stop the timer
  useEffect(() => {
    timerRef.current = window.setInterval(() => {
      setTimer((time) => time + 1);
    }, 1000);

    //code for the unmounting the ref
    return () => {
      stopTimer();
    };
  }, []);

  return (
    <>
      <h4>Mutable Ref example with the type</h4>
      Timer - {Timer}
      <br></br>
      <button onClick={() => stopTimer()}>Stop Timer</button>
    </>
  );
}

export default MutableRef;
