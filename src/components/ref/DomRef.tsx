import { useEffect, useRef } from "react";

function DomRef() {
  const inputRef = useRef<HTMLInputElement>(null);

  //if we don't want to write optional chaining then we have to use this syntax
  // const inputRef = useRef<HTMLInputElement>(null!);

  useEffect(() => {
    inputRef.current?.focus();
  }, []);

  return (
    <>
      <h4>useRef and it's type</h4>
      <input ref={inputRef} type="text"></input>
    </>
  );
}

export default DomRef;
