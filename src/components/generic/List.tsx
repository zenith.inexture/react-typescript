type ListType<G> = {
  items: G[];
  onClick: (value: G) => void;
};

//generic in class component
const List = <G extends string | number>({ items, onClick }: ListType<G>) => {
  return (
    <>
      <h4>Display number of itmes</h4>
      {items.map((item, index) => (
        <div key={index} onClick={() => onClick(item)}>
          {item}
        </div>
      ))}
    </>
  );
};

export default List;
