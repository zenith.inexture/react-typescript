import { useState, createContext } from "react";

//type of AuthUser that contain only name and email, so type should be string for both
export type AuthUser = {
  name: string;
  email: string;
};

//type for the state that we are passing from the value of provider
type UserContextType = {
  user: AuthUser | null;
  setUser: React.Dispatch<React.SetStateAction<AuthUser | null>>;
};

//type for provider that only render the children
type UserContextProviderProps = {
  children: React.ReactNode;
};

//context for the user
export const UserContext = createContext<UserContextType | null>(null);

//if we don't want to user option chaining the this is the way to do it
// export const UserContext = createContext<UserContextType | null>({} as UserContextType);

export const UserContextProvider = ({ children }: UserContextProviderProps) => {
  const [user, setUser] = useState<AuthUser | null>(null);
  return (
    // In case of passing the state from the provider the we have to defined the type of the state
    <UserContext.Provider value={{ user, setUser }}>
      {children}
    </UserContext.Provider>
  );
};
