import { createContext } from "react";
import { theme } from "./Theme";

//the context provide the context children
type ThemeContextProviderType = {
  children: React.ReactNode;
};

//created one context for the theme
export const ThemeContext = createContext(theme);

//this is theme-context-provider to return the children component that we have passed from context provider
export const ThemeContextProvider = ({
  children,
}: ThemeContextProviderType) => {
  //here the children is Box component passed as the child of the provider component
  console.log(children);
  return (
    <ThemeContext.Provider value={theme}>{children}</ThemeContext.Provider>
  );
};
