import { useContext } from "react";
import { UserContext } from "./UserContext";

const User = () => {
  //useContext for fetching the value provided by the userContext
  const userContext = useContext(UserContext);

  //function for set the user details and clear the user detils
  const handlelogin = () => {
    userContext &&
      userContext?.setUser({ name: "zenith", email: "zenith@gmail.com" });
  };
  const handlelogout = () => {
    //to access the value of provider we used to render in child component
    userContext && userContext?.setUser(null);
  };

  return (
    <>
      <button onClick={handlelogin}>Log in</button>
      <button onClick={handlelogout}>Log out</button>
      {userContext?.user && (
        <>
          <p>User Name is: {userContext?.user?.name}</p>
          <p>User Email is: {userContext?.user?.email}</p>
        </>
      )}
    </>
  );
};

export default User;
