//position props can be one of
//"left-center" | "left-top" | "left-bottom" | "center" | "center-top" | ............ combinations

//one way to type all the position is hardcore all position and type it
//but template literals way is help us to do this things

type HorizontalType = "left" | "center" | "right";
type VerticalType = "bottom" | "center" | "top";

//with the help of template literals we can merge value same as the template literals
//but it's made the center-center combination automatically
//so if we remove that center-center combination from our types then we have to use Exclude center-center

type PostionProps = {
  position: //this is allow us to remove center-center type form the combination that are created by the template-literals
  Exclude<`${HorizontalType}-${VerticalType}`, "center-center"> | "center";
};

function Toast({ position }: PostionProps) {
  return (
    <>
      <div>Toast Notification Position - {position}</div>
    </>
  );
}

export default Toast;
