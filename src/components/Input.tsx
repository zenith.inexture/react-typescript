import React from "react";

//with this we can attached the type of change event
type InputTypes = {
  value: string;
  handleChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const Input = (props: InputTypes) => {
  //handlechange function for the onChange input tag
  const handleInputChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    console.log(event);
  };
  return (
    <>
      <h4>onChange input type</h4>
      Input :{" "}
      <input
        type="text"
        value={props.value}
        onChange={handleInputChange}
      ></input>
    </>
  );
};

export default Input;
