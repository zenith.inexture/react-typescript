type RandomNumberType = {
  value: number;
};

//we can give restriction by giving the never property in typescript
//never -  prvert to write that props
type PositiveNumber = RandomNumberType & {
  isPositive: boolean;
  isNegative?: never;
  isZero?: never;
};
type NegativeNumber = RandomNumberType & {
  isPositive?: never;
  isNegative: boolean;
  isZero?: never;
};
type ZeroNumber = RandomNumberType & {
  isPositive?: never;
  isNegative?: never;
  isZero: boolean;
};

//type define for the value and other boolean type
type RandomNumberProps = PositiveNumber | NegativeNumber | ZeroNumber;

//function for display the random value is positive negative or zero
function RandomNumber({
  value,
  isPositive,
  isNegative,
  isZero,
}: RandomNumberProps) {
  return (
    <>
      <p>
        {value} is {isPositive && "positive"} {isNegative && "negative"}{" "}
        {isZero && "zero"}
      </p>
    </>
  );
}

export default RandomNumber;
