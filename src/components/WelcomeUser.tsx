//this is specify the type of props in our application
type WelcomeUserTypes = {
  name: string;
  age?: number; //Optional Chaining - if we don't pass age props then also it's works in this syntax
  //this is check types when we provide the age props form perent other wise it's ignore this age type
  isLoggedIn: boolean;
};

//we have to pass this type object to our props
const WelcomeUser = (props: WelcomeUserTypes) => {
  //like this we can define the default perameter of the props
  console.log(props.age); //undefined - bcz not passed
  const { age = 21 } = props;
  return (
    <>
      <h4>pass more then one value as a props and it's type</h4>
      {props.isLoggedIn ? (
        <h2>
          welcome {props.name}, your are {age} year old.
        </h2>
      ) : (
        <h2>welcome guest</h2>
      )}
    </>
  );
};

export default WelcomeUser;

//if we directy uses the props like porps.name then it's gives us error - what type of props u passed
//so, we nedd to info the type of props
