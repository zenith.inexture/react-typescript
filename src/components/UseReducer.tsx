import { useReducer } from "react";

//types of the state and action in this useReducer hook
type CounterState = {
  count: number;
};

//this type is for the increment and decrement
type UpdateAction = {
  type: "increment" | "decrement";
  payload: number;
};

//this type is only for the reset(in reset function there is no payload so we can make seperate type for this also)
type ResetAction = {
  type: "reset";
};

//mearge two type in the action bcz we have with payload method and one is without payload method
type CounterAction = UpdateAction | ResetAction;

//initialvalue for the state
const initialValues = {
  count: 0,
};

//this is the way to pass the type of the useReducer hook in reducer function
function reducer(state: CounterState, action: CounterAction) {
  switch (action.type) {
    case "increment":
      return { count: state.count + action.payload };
    case "decrement":
      return { count: state.count - action.payload };
    case "reset":
      return { count: initialValues.count };
    default:
      return state;
  }
}

function UseReducer() {
  const [state, dispatch] = useReducer(reducer, initialValues);
  return (
    <>
      <h4>useReducer hook with types</h4>
      Count : {state.count}
      <br></br>
      <button
        onClick={() => dispatch({ type: "increment", payload: 20 })}
        style={{ marginRight: "10px" }}
      >
        Increment by 20
      </button>
      <button onClick={() => dispatch({ type: "decrement", payload: 10 })}>
        Decrement by 10
      </button>
      <button onClick={() => dispatch({ type: "reset" })}>Reset to 0</button>
    </>
  );
}

export default UseReducer;
