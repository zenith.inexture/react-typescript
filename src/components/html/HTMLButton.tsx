type HTMLButtonType = {
  classname: "primary" | "secondary";
} & React.ComponentProps<"button">;
//React.ComponentProps<"button"> with the help of that i can use the children props in my child component
//And also use the rest of the props in my application with ...rest

//we can pass the ...rest in our element so with this we can access the other type
function HTMLButton({
  classname,
  children,
  ...restOfTheProps
}: HTMLButtonType) {
  return (
    <button className={classname} {...restOfTheProps}>
      {children}
    </button>
  );
}

export default HTMLButton;
