import React from "react";

//with the help of the componentprops we can spread it the all props in child component
type InputProps = React.ComponentProps<"input">;

function HTMLInput(props: InputProps) {
  return <input {...props} />;
}

export default HTMLInput;
