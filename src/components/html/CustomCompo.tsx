import React from "react";
import WelcomeUser from "../WelcomeUser";

//we can extract the type of other component also
//we can do it by importing and exporting also
function CustomCompo(props: React.ComponentProps<typeof WelcomeUser>) {
  return (
    <>
      <h4>Fetch the type of other component</h4>
      {props.isLoggedIn && <p>Hello {props.name}</p>}
    </>
  );
}
export default CustomCompo;
