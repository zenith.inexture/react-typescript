export type Name = {
  firstname: string;
  lastname: string;
};

//this is the way we can define the type of object in typescript
export type personType = {
  name: Name;
};

//if we working on the large project then we can't write the type in same file
//with the help of this we can reuse this type in any other files
