import { Name } from "./PersonType";

//this is the way to reuse the type in another component in application
type PersonListProps = {
  list: Array<Name>; //Name[] - both are the same (syntax is difference)
};

//in array declaration we have to pass [] sign in last of the element

const PersonList = (props: PersonListProps) => {
  return (
    <>
      <h4>type of list in props</h4>
      {props.list.map((i, index) => (
        <h2 key={index}>{i.firstname}</h2>
      ))}
    </>
  );
};

export default PersonList;
